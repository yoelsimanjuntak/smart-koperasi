<?php
class Koperasi extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/dashboard');
    } else {
      $ruser = GetLoggedUser();
      if($ruser[COL_IDROLE]!=ROLESUPER&&$ruser[COL_IDROLE]!=ROLEADMIN) {
        show_error('Maaf, akun anda tidak memiliki hak akses terhadap modul ini.');
        exit();
      }
    }
  }

  public function profil() {
    $ruser = GetLoggedUser();
    if($ruser[COL_IDROLE]!=ROLEADMIN) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $rdata = $this->db
    ->where(COL_UNIQ, $ruser[COL_IDKOPERASI])
    ->get(TBL_M_KOPERASI)
    ->row_array();

    $data['title'] = 'Profil Koperasi';
    $data['data'] = $rdata;
    $this->template->load('adminlte', 'site/koperasi/profil', $data);
  }

  public function registrasi() {
    $data['title'] = 'Permohonan Pendaftaran';
    $this->template->load('adminlte', 'site/koperasi/registrasi', $data);
  }

  public function registrasi_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $userStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_REGNAMA,null,null);
    $cols = array(COL_REGNAMA,COL_REGNIK,COL_REGNOHP,'ReffNama','SponsorNama');

    if($ruser[COL_IDROLE]!=ROLESUPER) {
      $this->db->where(TBL_T_ANGGOTA_REG.'.'.COL_IDKOPERASI, $ruser[COL_IDKOPERASI]);
    }
    $queryAll = $this->db
    ->where(COL_REGISTERDAFTAR, 0)
    ->get(TBL_T_ANGGOTA_REG);

    $i = 0;
    foreach($cols as $item){
      if($item=='ReffNama') {
        $item = 'reff.AngNama';
      } else if($item=='SponsorNama') {
        $item = 'sponsor.AngNama';
      } else {
        $item = 't_anggota_reg.'.$item;
      }
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    if($ruser[COL_IDROLE]!=ROLESUPER) {
      $this->db->where(TBL_T_ANGGOTA_REG.'.'.COL_IDKOPERASI, $ruser[COL_IDKOPERASI]);
    }
    $q = $this->db
    ->select('t_anggota_reg.*, reff.AngNama as ReffNama, sponsor.AngNama as SponsorNama')
    ->join(TBL_T_ANGGOTA.' reff','reff.'.COL_UNIQ." = ".TBL_T_ANGGOTA_REG.".".COL_IDREFERRAL,"left")
    ->join(TBL_T_ANGGOTA.' sponsor','sponsor.'.COL_UNIQ." = ".TBL_T_ANGGOTA_REG.".".COL_IDSPONSOR,"left")
    ->where(COL_REGISTERDAFTAR, 0)
    ->get_compiled_select(TBL_T_ANGGOTA_REG, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/koperasi/reg-process/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-popup"><i class="fas fa-check-circle"></i>&nbsp;PROSES</a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_REGNAMA],
        $r[COL_REGNIK],
        $r[COL_REGNOHP],
        $r['ReffNama'],
        $r['SponsorNama'],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function reg_process($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_T_ANGGOTA_REG)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid!';
      exit();
    }

    $rsponsor = $this->db
    ->where(COL_UNIQ, $rdata[COL_IDSPONSOR])
    ->get(TBL_T_ANGGOTA)
    ->row_array();
    $rreff = $this->db
    ->where(COL_UNIQ, $rdata[COL_IDREFERRAL])
    ->get(TBL_T_ANGGOTA)
    ->row_array();

    if(empty($rsponsor) || empty($rreff)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "png|jpg|jpeg";
      $config['max_size']	= 512000;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_REGUSERNAME))
      ->or_where(COL_EMAIL, $this->input->post(COL_REGEMAIL))
      ->get(TBL__USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, username / email telah terdaftar. Silakan gunakan username / email lain.');
        exit();
      }

      $numPayment = 0;
      $numFeeAdm = !empty(GetSetting('AMT_ADMFEE'))?toNum(GetSetting('AMT_ADMFEE')):0;
      $numBonusReff = !empty(GetSetting('AMT_BONUS_REFERRAL'))&&!empty(GetSetting('NUM_REFFDEPTH'))?toNum(GetSetting('AMT_BONUS_REFERRAL'))*toNum(GetSetting('NUM_REFFDEPTH')):0;
      $numBonusSponsor = !empty(GetSetting('AMT_BONUS_SPONSOR'))?toNum(GetSetting('AMT_BONUS_SPONSOR')):0;
      $numPayment = $numFeeAdm+$numBonusReff+$numBonusSponsor;

      $datMember = array(
        COL_IDKOPERASI=>$rreff[COL_IDKOPERASI],
        COL_IDREFERRAL=>$rreff[COL_UNIQ],
        COL_IDSPONSOR=>$rsponsor[COL_UNIQ],
        COL_ANGNAMA=>$this->input->post(COL_REGNAMA),
        COL_ANGNIK=>$this->input->post(COL_REGNIK),
        COL_ANGNOHP=>$this->input->post(COL_REGNOHP),
        COL_ANGALAMAT=>$this->input->post(COL_REGALAMAT),
        COL_ANGPROFILEPIC=>$rdata[COL_REGPROFILEPIC],
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );
      $datUser = array(
        COL_IDKOPERASI=>$rreff[COL_IDKOPERASI],
        COL_IDROLE=>ROLEUSER,
        COL_NAME=>$this->input->post(COL_REGNAMA),
        COL_USERNAME=>$this->input->post(COL_REGUSERNAME),
        COL_EMAIL=>$this->input->post(COL_REGEMAIL),
        COL_PASSWORD=>$rdata[COL_REGPASSWORD],
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );
      $datTrans = array(
        COL_TRANSTANGGAL=>date('Y-m-d'),
        COL_TRANSNAMA=>'Pendaftaran Anggota',
        COL_TRANSJUMLAH=>$numPayment,
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $numSisa = $numPayment;
      $datSaldo = array();
      $datJurnal = array();
      $refs = $rreff;

      /* Jurnal Biaya Adm. */
      $datJurnal[] = array(
        COL_IDKOPERASI=>$datMember[COL_IDKOPERASI],
        COL_JURTANGGAL=>date('Y-m-d'),
        COL_JURPOSISI=>'K',
        COL_JURREKENING=>'Biaya Adm. Pendaftaran',
        COL_JURJUMLAH=>$numPayment,
        COL_JURKETERANGAN=>'Pendaftaran Anggota an. '.$datMember[COL_ANGNAMA],
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );
      /* Jurnal Biaya Adm. */

      while(!empty($refs)) {
        /* Bonus Refereral */
        $datSaldo[] = array(
          COL_IDANGGOTA=>$refs[COL_UNIQ],
          COL_SALJUMLAH=>!empty(GetSetting('AMT_BONUS_REFERRAL'))?toNum(GetSetting('AMT_BONUS_REFERRAL')):0,
          COL_SALKETERANGAN=>'Bonus Referral Anggota an. '.$datMember[COL_ANGNAMA],
          COL_CREATEDON=>date('Y-m-d H:i:s'),
          COL_CREATEDBY=>$ruser[COL_USERNAME]
        );
        $datJurnal[] = array(
          COL_IDKOPERASI=>$datMember[COL_IDKOPERASI],
          COL_JURTANGGAL=>date('Y-m-d'),
          COL_JURPOSISI=>'D',
          COL_JURREKENING=>'Simpanan Anggota',
          COL_JURJUMLAH=>!empty(GetSetting('AMT_BONUS_REFERRAL'))?toNum(GetSetting('AMT_BONUS_REFERRAL')):0,
          COL_JURKETERANGAN=>'Bonus Referral Anggota an. '.$refs[COL_ANGNAMA],
          COL_CREATEDON=>date('Y-m-d H:i:s'),
          COL_CREATEDBY=>$ruser[COL_USERNAME]
        );
        /* Bonus Refereral */
        $numSisa = $numSisa-(!empty(GetSetting('AMT_BONUS_REFERRAL'))?toNum(GetSetting('AMT_BONUS_REFERRAL')):0);

        if(!empty($refs[COL_IDREFERRAL])) {
          $refs = $this->db
          ->where(COL_UNIQ, $refs[COL_IDREFERRAL])
          ->get(TBL_T_ANGGOTA)
          ->row_array();
        } else {
          $refs = null;
        }
      }

      /* Bonus Sponsor */
      $datSaldo[] = array(
        COL_IDANGGOTA=>$rsponsor[COL_UNIQ],
        COL_SALJUMLAH=>$numBonusSponsor,
        COL_SALKETERANGAN=>'Bonus Sponsor Anggota an. '.$datMember[COL_ANGNAMA],
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );
      $datJurnal[] = array(
        COL_IDKOPERASI=>$datMember[COL_IDKOPERASI],
        COL_JURTANGGAL=>date('Y-m-d'),
        COL_JURPOSISI=>'D',
        COL_JURREKENING=>'Simpanan Anggota',
        COL_JURJUMLAH=>$numBonusSponsor,
        COL_JURKETERANGAN=>'Bonus Sponsor Anggota an. '.$rsponsor[COL_ANGNAMA],
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );
      /* Bonus Sponsor */
      $numSisa = $numSisa-$numBonusSponsor;

      $this->db->trans_begin();
      try {
        if(!empty($_FILES['file']['name'])) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $datMember[COL_ANGPROFILEPIC] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_T_ANGGOTA, $datMember);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idMember = $this->db->insert_id();
        $datUser[COL_IDMEMBER] = $idMember;
        $datTrans[COL_TRANSKETERANGAN]='Pendaftaran Anggota ID#'.$idMember;

        /* Saldo Awal */
        $datSaldo[] = array(
          COL_IDANGGOTA=>$idMember,
          COL_SALJUMLAH=>$numFeeAdm,
          COL_SALKETERANGAN=>'Pendaftaran Anggota an. '.$datMember[COL_ANGNAMA],
          COL_CREATEDON=>date('Y-m-d H:i:s'),
          COL_CREATEDBY=>$ruser[COL_USERNAME]
        );
        $datJurnal[] = array(
          COL_IDKOPERASI=>$datMember[COL_IDKOPERASI],
          COL_JURTANGGAL=>date('Y-m-d'),
          COL_JURPOSISI=>'D',
          COL_JURREKENING=>'Simpanan Anggota',
          COL_JURJUMLAH=>$numFeeAdm,
          COL_JURKETERANGAN=>'Simpanan Awal Anggota an. '.$datMember[COL_ANGNAMA],
          COL_CREATEDON=>date('Y-m-d H:i:s'),
          COL_CREATEDBY=>$ruser[COL_USERNAME]
        );
        /* Saldo Awal */
        $numSisa = $numSisa-$numFeeAdm;

        if($numSisa > 0) {
          $datJurnal[] = array(
            COL_IDKOPERASI=>$datMember[COL_IDKOPERASI],
            COL_JURTANGGAL=>date('Y-m-d'),
            COL_JURPOSISI=>'D',
            COL_JURREKENING=>'Kas',
            COL_JURJUMLAH=>$numSisa,
            COL_JURKETERANGAN=>'Sisa Pendaftaran Anggota an. '.$datMember[COL_ANGNAMA],
            COL_CREATEDON=>date('Y-m-d H:i:s'),
            COL_CREATEDBY=>$ruser[COL_USERNAME]
          );
        }

        $res = $this->db->insert(TBL_T_TRANSAKSI, $datTrans);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idTrans = $this->db->insert_id();
        for($i=0; $i<count($datSaldo); $i++) {
          $datSaldo[$i][COL_IDTRANSAKSI] = $idTrans;
        }
        for($i=0; $i<count($datJurnal); $i++) {
          $datJurnal[$i][COL_IDTRANSAKSI] = $idTrans;
          $datJurnal[$i][COL_JURNOMOR] = 'T-'.str_pad($idTrans,5,"0",STR_PAD_LEFT);
          $datJurnal[$i][COL_JURREFERENSI] = 'Transaksi ID#'.$idTrans;
        }

        $res = $this->db->insert_batch(TBL_T_SALDO, $datSaldo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert_batch(TBL_T_JURNAL, $datJurnal);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert(TBL__USERS, $datUser);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_T_ANGGOTA_REG, array(COL_REGISTERDAFTAR=>1));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Proses anggota an. '.$datMember[COL_ANGNAMA].' berhasil!', array('redirect'=>site_url('site/koperasi/registrasi')));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

    } else {
      $data['data'] = $rdata;
      $data['rsponsor'] = $rsponsor;
      $data['rreff'] = $rreff;
      $this->load->view('site/koperasi/registrasi-process', $data);
    }
  }

  public function anggota() {
    $ruser = GetLoggedUser();
    if($ruser[COL_IDROLE]!=ROLESUPER && $ruser[COL_IDROLE]!=ROLEADMIN) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $data['title'] = 'Daftar Anggota';
    $this->template->load('adminlte', 'site/koperasi/anggota', $data);
  }

  public function anggota_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $userStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_ANGNAMA,COL_ANGNIK,'ReffNama','SponsorNama');
    $cols = array(COL_ANGNAMA,COL_ANGNIK,'ReffNama','SponsorNama');

    $queryAll = $this->db
    ->where(COL_IDKOPERASI, $ruser[COL_IDKOPERASI])
    ->get(TBL_T_ANGGOTA);

    $i = 0;
    foreach($cols as $item){
      if($item=='ReffNama') {
        $item = 'reff.AngNama';
      } else if($item=='SponsorNama') {
        $item = 'sponsor.AngNama';
      } else {
        $item = 't_anggota.'.$item;
      }
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('t_anggota.*, reff.AngNama as ReffNama, sponsor.AngNama as SponsorNama')
    ->join(TBL_T_ANGGOTA.' reff','reff.'.COL_UNIQ." = ".TBL_T_ANGGOTA.".".COL_IDREFERRAL,"left")
    ->join(TBL_T_ANGGOTA.' sponsor','sponsor.'.COL_UNIQ." = ".TBL_T_ANGGOTA.".".COL_IDSPONSOR,"left")
    ->where(TBL_T_ANGGOTA.'.'.COL_IDKOPERASI, $ruser[COL_IDKOPERASI])
    ->get_compiled_select(TBL_T_ANGGOTA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        $r[COL_ANGNAMA],
        $r[COL_ANGNIK],
        $r['ReffNama'],
        $r['SponsorNama'],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }
}
?>

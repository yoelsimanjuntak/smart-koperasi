<?php
class User extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  public function login() {
    if(IsLogin()) {
      redirect('site/user/dashboard');
    }

    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'Username',
          'label' => 'Username',
          'rules' => 'required'
        ),
        array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required'
        )
      ));

      if($this->form_validation->run()) {
        $username = $this->input->post(COL_USERNAME);
        $password = $this->input->post(COL_PASSWORD);

        $ruser = $this->db
        ->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_IDROLE,"left")
        ->where(COL_USERNAME,$username)
        ->where(COL_PASSWORD,md5($password))
        ->get(TBL__USERS)
        ->row_array();

        if(empty($ruser)) {
          ShowJsonError('Maaf, username / password yang anda masukkan tidak valid. Silakan coba kembali.');
          exit();
        }
        if($ruser[COL_IDROLE]!=ROLESUPER) {
          if($ruser[COL_ISACTIVE]!=1) {
            ShowJsonError('Maaf, akun anda untuk sementara di SUSPEND. Silakan hubungi administrator.');
            exit();
          }
        }

        SetLoginSession($ruser);
        ShowJsonSuccess('Selamat datang kembali, <strong>'.$ruser[COL_NAME].'</strong>!', array('redirect'=>site_url('site/user/dashboard')));
        exit();

      } else {
        ShowJsonError('Maaf, username / password yang anda masukkan tidak valid. Silakan coba kembali.');
        exit();
      }
    } else {
      $this->load->view('site/user/login');
    }
  }

  public function logout(){
    UnsetLoginSession();
    redirect(site_url());
  }

  public function dashboard() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $data['title'] = 'Dashboard';
    $this->template->load('adminlte', 'site/user/dashboard', $data);
  }

  public function changepassword() {
    $data['data'] = array();
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'NewPassword',
          'label' => 'NewPassword',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[NewPassword]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      if($ruser[COL_PASSWORD] != md5($this->input->post('OldPassword'))) {
        ShowJsonError('Password Lama tidak valid.');
        return false;
      }

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post('NewPassword'))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password anda berhasil diperbarui.', array('redirect'=>site_url('site/user/logout')));
        return false;
      } else {
        ShowJsonError(validation_errors());
        return false;
      }
    } else {
      $this->load->view('site/user/changepassword', $data);
    }
  }

  public function tree($id='') {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $data['title'] = 'Pohon Jaringan';
    $data['id'] = $id;
    $this->template->load('adminlte', 'site/user/member-tree', $data);
  }

  public function register($id) {
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    $ruser = GetLoggedUser();
    $rsponsor = $this->db
    ->where(COL_UNIQ, $ruser[COL_IDMEMBER])
    ->get(TBL_T_ANGGOTA)
    ->row_array();
    $rreff = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_T_ANGGOTA)
    ->row_array();

    if(empty($rsponsor) || empty($rreff)) {
      show_error('Parameter tidak valid!');
      exit();
    }
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "png|jpg|jpeg";
      $config['max_size']	= 512000;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_USERNAME))
      ->or_where(COL_EMAIL, $this->input->post(COL_EMAIL))
      ->get(TBL__USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, username / email telah terdaftar. Silakan gunakan username / email lain.');
        exit();
      }

      if($this->input->post(COL_REGPASSWORD) != $this->input->post('ConfirmPassword')) {
        ShowJsonError('Harap ketikkan KONFIRMASI PASSWORD anda dengan benar!');
        exit();
      }

      $data = array(
        COL_IDKOPERASI=>$rreff[COL_IDKOPERASI],
        COL_IDREFERRAL=>$this->input->post(COL_IDREFERRAL),
        COL_IDSPONSOR=>$this->input->post(COL_IDSPONSOR),
        COL_REGNAMA=>$this->input->post(COL_REGNAMA),
        COL_REGNIK=>$this->input->post(COL_REGNIK),
        COL_REGNOHP=>$this->input->post(COL_REGNOHP),
        COL_REGEMAIL=>$this->input->post(COL_REGEMAIL),
        COL_REGALAMAT=>$this->input->post(COL_REGALAMAT),
        COL_REGUSERNAME=>$this->input->post(COL_REGUSERNAME),
        COL_REGPASSWORD=>md5($this->input->post(COL_REGPASSWORD)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES['file']['name'])) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $data[COL_REGPROFILEPIC] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_T_ANGGOTA_REG, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Pendaftaran an. '.$data[COL_REGNAMA].' berhasil!', array('redirect'=>site_url('site/user/members/reg')));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      $data['title'] = 'Pendaftaran Anggota';
      $data['rsponsor'] = $rsponsor;
      $data['rreff'] = $rreff;
      $this->template->load('adminlte', 'site/user/member-register', $data);
    }
  }

  public function members($type='') {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $data['title'] = 'Daftar Anggota '.($type=='reg'?'Belum Terdaftar':'Terdaftar');
    $data['type'] = $type;
    $this->template->load('adminlte', 'site/user/member-list', $data);
  }

  public function members_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $userStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_ANGNAMA,COL_ANGNIK,'ReffNama');
    $cols = array(COL_ANGNAMA,COL_ANGNIK,'ReffNama');

    $queryAll = $this->db
    ->where(COL_IDSPONSOR, $ruser[COL_IDMEMBER])
    ->get(TBL_T_ANGGOTA);

    $i = 0;
    foreach($cols as $item){
      if($item=='ReffNama') {
        $item = 'reff.AngNama';
      } else {
        $item = 't_anggota.'.$item;
      }
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('t_anggota.*, reff.AngNama as ReffNama')
    ->join(TBL_T_ANGGOTA.' reff','reff.'.COL_UNIQ." = ".TBL_T_ANGGOTA.".".COL_IDREFERRAL,"left")
    ->where(TBL_T_ANGGOTA.'.'.COL_IDSPONSOR, $ruser[COL_IDMEMBER])
    ->get_compiled_select(TBL_T_ANGGOTA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        $r[COL_ANGNAMA],
        $r[COL_ANGNIK],
        $r['ReffNama'],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function member_reg_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $userStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_REGNAMA,COL_REGNIK,'ReffNama');
    $cols = array(COL_REGNAMA,COL_REGNIK,'ReffNama');

    $queryAll = $this->db
    ->where(COL_IDSPONSOR, $ruser[COL_IDMEMBER])
    ->where(COL_REGISTERDAFTAR, 0)
    ->get(TBL_T_ANGGOTA_REG);

    $i = 0;
    foreach($cols as $item){
      if($item=='ReffNama') {
        $item = 'reff.AngNama';
      } else {
        $item = 't_anggota_reg.'.$item;
      }
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('t_anggota_reg.*, reff.AngNama as ReffNama')
    ->join(TBL_T_ANGGOTA.' reff','reff.'.COL_UNIQ." = ".TBL_T_ANGGOTA_REG.".".COL_IDREFERRAL,"left")
    ->where(TBL_T_ANGGOTA_REG.'.'.COL_IDSPONSOR, $ruser[COL_IDMEMBER])
    ->where(TBL_T_ANGGOTA_REG.'.'.COL_REGISTERDAFTAR, 0)
    ->get_compiled_select(TBL_T_ANGGOTA_REG, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        $r[COL_REGNAMA],
        $r[COL_REGNIK],
        $r['ReffNama'],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function deposit_detail() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $data['title'] = 'Riwayat Simpanan';
    $this->template->load('adminlte', 'site/user/member-deposit-history', $data);
  }

  public function deposit_detail_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $userStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(COL_SALKETERANGAN,null,COL_CREATEDON);
    $cols = array(COL_SALKETERANGAN);
    $anggota_ = array();

    if($ruser[COL_IDROLE]==ROLEUSER) {
      $this->db->where(COL_IDANGGOTA, $ruser[COL_IDMEMBER]);
    }
    if($ruser[COL_IDROLE]==ROLEADMIN) {
      $ranggota_ = $this->db
      ->where(COL_IDKOPERASI, $ruser[COL_IDKOPERASI])
      ->get(TBL_T_ANGGOTA)
      ->result_array();
      foreach($ranggota_ as $ang) {
        $anggota_[] = $ang[COL_UNIQ];
      }
      $this->db->where_in(COL_IDANGGOTA, $anggota_);
    }
    $queryAll = $this->db->get(TBL_T_SALDO);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    if($ruser[COL_IDROLE]==ROLEUSER) {
      $this->db->where(COL_IDANGGOTA, $ruser[COL_IDMEMBER]);
    }
    if($ruser[COL_IDROLE]==ROLEADMIN) {
      $this->db->where_in(COL_IDANGGOTA, $anggota_);
    }

    $q = $this->db->get_compiled_select(TBL_T_SALDO, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        $r[COL_SALKETERANGAN],
        number_format($r[COL_SALJUMLAH]),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }
}

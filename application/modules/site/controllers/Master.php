<?php
class Master extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/dashboard');
    } else {
      $ruser = GetLoggedUser();
      if($ruser[COL_IDROLE]!=ROLESUPER) {
        show_error('Maaf, akun anda tidak memiliki hak akses terhadap modul ini.');
        exit();
      }
    }
  }

  public function koperasi() {
    $data['title'] = "Daftar Koperasi";
    $this->template->load('adminlte', 'master/koperasi', $data);
  }

  public function koperasi_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_KOPNAMA=>'desc');
    $orderables = array(null,COL_KOPNAMA);
    $cols = array(COL_KOPNAMA);

    $queryAll = $this->db
    ->where(COL_KOPISDELETED, 0)
    ->get(TBL_M_KOPERASI);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('*, (select count(*) from t_anggota where t_anggota.IDKoperasi=m_koperasi.Uniq) as NumAnggota')
    ->where(COL_KOPISDELETED, 0)
    ->order_by(TBL_M_KOPERASI.".".COL_KOPNAMA, 'asc')
    ->get_compiled_select(TBL_M_KOPERASI, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/koperasi-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/koperasi-detail/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary" data-toggle="tooltip" data-placement="top" data-title="Rincian"><i class="fas fa-search"></i></a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_KOPNAMA],
        number_format($r['NumAnggota']),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function koperasi_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_KOPNAMA => $this->input->post(COL_KOPNAMA),
        COL_KOPSLUG => slugify($this->input->post(COL_KOPNAMA)),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_M_KOPERASI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function koperasi_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_M_KOPERASI, array(COL_KOPISDELETED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function koperasi_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_M_KOPERASI)
    ->row_array();
    if(empty($rdata)) {
      ShowJsonError('Data tidak ditemukan!');
      exit();
    }

    $config['upload_path'] = MY_UPLOADPATH.'koperasi/';
    $config['allowed_types'] = "png|jpg|jpeg";
    $config['max_size']	= 512000;
    $config['max_width']  = 4000;
    $config['max_height']  = 4000;
    $config['overwrite'] = FALSE;
    $this->load->library('upload',$config);

    $data = array(
      COL_KOPNAMA => $this->input->post(COL_KOPNAMA),
      COL_KOPSLUG => slugify($this->input->post(COL_KOPNAMA)),
      COL_KOPALAMAT => $this->input->post(COL_KOPALAMAT),
      COL_KOPNOTELP => $this->input->post(COL_KOPNOTELP),
      COL_KOPPROFIL => $this->input->post(COL_KOPPROFIL),
      COL_UPDATEDBY => $ruser[COL_USERNAME],
      COL_UPDATEDON => date('Y-m-d H:i:s')
    );

    $this->db->trans_begin();
    try {
      if(!empty($_FILES['file']['name'])) {
        $res = $this->upload->do_upload('file');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          throw new Exception($err);
        }
        $upl = $this->upload->data();
        $data[COL_KOPLOGO] = $upl['file_name'];
      }

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_M_KOPERASI, $data);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Data koperasi berhasil diperbarui!', array('redirect'=>site_url('site/master/koperasi-detail/'.$id)));
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function koperasi_detail($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_M_KOPERASI)
    ->row_array();
    if(empty($rdata)) {
      show_error('Data tidak ditemukan!');
      exit();
    }

    $data['title'] = $rdata[COL_KOPNAMA];
    $data['data'] = $rdata;
    $this->template->load('adminlte', 'master/koperasi-detail', $data);
  }

  public function koperasi_detail_change($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_M_KOPERASI)
    ->row_array();
    if(empty($rdata)) {
      show_error('Data tidak ditemukan!');
      exit();
    }

    $rdet = $this->db
    ->where(COL_IDKOPERASI, $rdata[COL_UNIQ])
    ->get(TBL_M_KOPERASI_DET)
    ->result_array();

    $config['upload_path'] = MY_UPLOADPATH.'koperasi/';
    $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
    $config['max_size']	= 512000;
    $config['max_width']  = 4000;
    $config['max_height']  = 4000;
    $config['overwrite'] = FALSE;
    $this->load->library('upload',$config);

    $this->db->trans_begin();
    try {
      foreach($rdet as $d) {
        $data_ = array(
          COL_KOPTEKS=>$this->input->post("_input_detail_".$d[COL_UNIQ])
        );

        if(!empty($_FILES['_file_detail_'.$d[COL_UNIQ]]['name'])) {
          $res = $this->upload->do_upload('_file_detail_'.$d[COL_UNIQ]);
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $data_[COL_KOPFILE] = $upl['file_name'];
        }

        $res = $this->db->where(COL_UNIQ, $d[COL_UNIQ])->update(TBL_M_KOPERASI_DET, $data_);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Data koperasi berhasil diperbarui!', array('redirect'=>site_url('site/master/koperasi-detail/'.$id)));
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function user() {
    $data['title'] = "Daftar Pengguna";
    $this->template->load('adminlte', 'master/user', $data);
  }

  public function user_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $userStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_USERNAME,COL_EMAIL,COL_NAME,COL_ROLENAME,null,COL_CREATEDON);
    $cols = array(COL_USERNAME,COL_EMAIL,COL_NAME,COL_ROLENAME);

    $queryAll = $this->db
    //->where(COL_IDROLE, ROLEUSER)
    ->get(TBL__USERS);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($userStatus)) {
      if($userStatus==1) {
        $this->db->where(COL_ISACTIVE, 1);
      } else {
        $this->db->where(COL_ISACTIVE, 0);
      }
    }

    $this->db->order_by(TBL__ROLES.'.'.COL_ROLEID);
    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_IDROLE,"left")
    //->where(COL_IDROLE, ROLEUSER)
    ->get_compiled_select(TBL__USERS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/user-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Ubah"><i class="fas fa-edit"></i></a>&nbsp;';
      if($r[COL_ISACTIVE]==1) {
        $htmlBtn .= '<a href="'.site_url('site/master/user-activation/'.$r[COL_UNIQ].'/0').'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Suspend"><i class="fas fa-times-circle"></i></a>';
      } else {
        $htmlBtn .= '<a href="'.site_url('site/master/user-activation/'.$r[COL_UNIQ].'/1').'" class="btn btn-xs btn-outline-success btn-action" data-toggle="tooltip" data-placement="top" data-title="Aktifkan"><i class="fas fa-check-circle"></i></a>';
      }
      //$htmlBtn .= '&nbsp;<a href="'.site_url('site/master/user-resetpassword/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info btn-edit" data-toggle="tooltip" data-placement="top" data-title="Reset Password"><i class="fas fa-sync"></i></a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_USERNAME],
        $r[COL_EMAIL],
        $r[COL_NAME],
        $r[COL_ROLENAME],
        ($r[COL_ISACTIVE]==1?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger"></i>'),
        //date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function user_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_USERNAME))
      ->or_where(COL_EMAIL, $this->input->post(COL_EMAIL))
      ->get(TBL__USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, username / email telah terdaftar. Silakan gunakan username / email lain.');
        exit();
      }

      if($this->input->post(COL_PASSWORD) != $this->input->post('ConfirmPassword')) {
        ShowJsonError('Harap ketikkan KONFIRMASI PASSWORD anda dengan benar!');
        exit();
      }

      $data = array(
        COL_IDROLE=>$this->input->post(COL_IDROLE),
        COL_NAME=>$this->input->post(COL_NAME),
        COL_USERNAME=>$this->input->post(COL_USERNAME),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_PASSWORD=>md5($this->input->post(COL_PASSWORD)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );
      if($data[COL_IDROLE]==ROLEADMIN) {
        if(empty($this->input->post(COL_IDKOPERASI))) {
          ShowJsonError('Harap pilih Koperasi untuk pengguna dengan kategori Administrator!');
          exit();
        }

        $data[COL_IDKOPERASI] = $this->input->post(COL_IDKOPERASI);
      }

      $res = $this->db->insert(TBL__USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Data Pengguna <strong>'.$data[COL_NAME].'</strong> berhasil ditambahkan.');
      exit();
    } else {
      $this->load->view('site/master/user-form');
    }
  }

  public function user_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid!';
      exit();
    }

    if(!empty($_POST)) {
      if(!empty($this->input->post(COL_PASSWORD)) && $this->input->post(COL_PASSWORD) != $this->input->post('ConfirmPassword')) {
        ShowJsonError('Harap ketikkan KONFIRMASI PASSWORD anda dengan benar!');
        exit();
      }

      $data = array(
        COL_IDROLE=>$this->input->post(COL_IDROLE),
        COL_NAME=>$this->input->post(COL_NAME),
        COL_USERNAME=>$this->input->post(COL_USERNAME),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );
      if($data[COL_IDROLE]==ROLEADMIN) {
        if(empty($this->input->post(COL_IDKOPERASI))) {
          ShowJsonError('Harap pilih Koperasi untuk pengguna dengan kategori Administrator!');
          exit();
        }

        $data[COL_IDKOPERASI] = $this->input->post(COL_IDKOPERASI);
      }
      if(!empty($this->input->post(COL_PASSWORD))) {
        $data[COL_PASSWORD]=md5($this->input->post(COL_PASSWORD));
      }

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL__USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Data Pengguna <strong>'.$data[COL_NAME].'</strong> berhasil diperbarui.');
      exit();
    } else {
      $this->load->view('site/master/user-form', array('data'=>$rdata));
    }
  }

  public function user_activation($id, $stat=0) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL__USERS, array(COL_ISACTIVE=>$stat==1?1:0));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL');
    exit();
  }
}
?>

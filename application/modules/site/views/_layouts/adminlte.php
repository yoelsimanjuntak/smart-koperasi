
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?=!empty($title) ? $this->setting_web_name.' - '.$title : $this->setting_web_name?></title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/fonts/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontawesome-pro/web/css/all.min.css" />

  <!-- Ionicons -->
  <link href="<?=base_url()?>assets/fonts/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

  <!-- JQUERY -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

  <!-- Upload file -->
  <!--<link href="<?=base_url()?>assets/css/uploadfile.css" rel="stylesheet" type="text/css" />-->

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <!-- Select 2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/datatable/media/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


  <!-- datatable reorder _ buttons ext + resp + print -->
  <!--<script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>-->
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
  <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

  <!-- daterange picker -->
  <script src="<?=base_url()?>assets/js/moment.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">

  <!-- WYSIHTML5 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- Toastr -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">

  <!-- Custom CSS -->
  <!--<link href="<?=base_url()?>assets/css/my.css" rel="stylesheet" type="text/css" />-->

  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_logo?>>
  <style>
  .no-js #loader { display: none;  }
  .js #loader { display: block; position: absolute; left: 100px; top: 0; }
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url().$this->setting_web_preloader?>) center no-repeat #fff;
  }

  @media (max-width: 767px) {
      .sidebar-toggle {
          font-size: 3vw !important;
      }
  }

  .form-group .control-label {
      text-align: right;
      line-height: 2;
  }
  .nowrap {
    white-space: nowrap;
  }
  .va-middle {
    vertical-align: middle !important;
  }
  .border-0 td {
    border: 0!important;
  }
  td.dt-body-right {
    text-align: right !important;
  }
  .custom-file-label {
    overflow-x: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    padding-right: 75px;
  }
  .tab-content>.active {
    display: block;
    border-bottom: 1px solid #dee2e6;
    border-left: 1px solid #dee2e6;
    border-right: 1px solid #dee2e6;
    background: #fff;
  }
  a.nav-link.active {
    font-weight: bold;
  }
  .nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover {
    background: #fff;
  }
  </style>
</head>
<body class="sidebar-mini sidebar-collapse layout-fixed">
<div class="se-pre-con"></div>
<div class="wrapper">
  <?php
  $ruser = GetLoggedUser();
  $displayname = $ruser ? strtoupper($ruser[COL_NAME]) : "TAMU";
  $displaypicture = MY_IMAGEURL.'img-user.jpg';
  if($ruser) {
    $displaypicture = MY_IMAGEURL.'img-user.jpg';
  }
  if(strlen($displayname)>15) {
    //$displayname = strtok($displayname, " ");
  }
  ?>

  <nav class="main-header navbar navbar-expand navbar-info navbar-dark">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#">
          <i class="fas fa-bars"></i>&nbsp;
          <strong><?=strtoupper($this->setting_web_name)?></strong>&nbsp;<span class="d-none d-sm-inline"><?=$this->setting_web_desc?></span>
        </a>
      </li>
    </ul>
  </nav>
  <aside class="main-sidebar sidebar-light-info elevation-2">
    <a href="<?=site_url()?>" class="brand-link bg-info text-center" style="padding: 0.5rem 0.5rem !important">
      <img src="<?=base_url().$this->setting_web_logo?>" alt="Logo" class="brand-image p-1" style="float: none !important; max-height: 43px !important; margin-left: .5rem !important; margin-right: .5rem !important; border-radius: 50% !important">
    </a>
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=$displaypicture?>" class="img-circle elevation-2" alt="Your Profile Image">
        </div>
        <div class="info pt-1">
          <a href="#" class="d-block" style="line-height: 1 !important">
            <strong><?=$displayname?></strong><br />
            <small><?=$ruser[COL_ROLENAME]?></small>
          </a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?=site_url('site/user/dashboard')?>" class="nav-link">
              <i class="nav-icon fa fa-home"></i>
              <p>DASHBOARD</p>
            </a>
          </li>
          <?php
          if($ruser[COL_IDROLE] == ROLESUPER) {
            ?>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle"></i><p>MASTER<i class="fa fa-angle-left right"></i></p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?=site_url('site/master/koperasi')?>" class="nav-link">
                    <i class="fa fa-angle-right nav-icon"></i>
                    <p>Koperasi</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=site_url('site/master/user')?>" class="nav-link">
                    <i class="fa fa-angle-right nav-icon"></i>
                    <p>Pengguna</p>
                  </a>
                </li>
              </ul>
            </li>
            <?php
          }
          if($ruser[COL_IDROLE] == ROLESUPER || $ruser[COL_IDROLE] == ROLEADMIN) {
            ?>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle"></i><p>KOPERASI<i class="fa fa-angle-left right"></i></p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?=site_url('site/koperasi/profil')?>" class="nav-link">
                    <i class="fa fa-angle-right nav-icon"></i>
                    <p>Profil</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=site_url('site/koperasi/registrasi')?>" class="nav-link">
                    <i class="fa fa-angle-right nav-icon"></i>
                    <p>Pendaftaran</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=site_url('site/koperasi/anggota')?>" class="nav-link">
                    <i class="fa fa-angle-right nav-icon"></i>
                    <p>Anggota</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=site_url('site/koperasi/transaksi')?>" class="nav-link">
                    <i class="fa fa-angle-right nav-icon"></i>
                    <p>Transaksi</p>
                  </a>
                </li>
              </ul>
            </li>
            <?php
          }
          if($ruser[COL_IDROLE] == ROLEUSER) {
            ?>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-users"></i><p>ANGGOTA<i class="fa fa-angle-left right"></i></p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?=site_url('site/user/members/reg')?>" class="nav-link">
                    <i class="fa fa-angle-right nav-icon"></i>
                    <p>Pendaftaran</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=site_url('site/user/members')?>" class="nav-link">
                    <i class="fa fa-angle-right nav-icon"></i>
                    <p>Referensi</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="<?=site_url('site/user/tree')?>" class="nav-link">
                <i class="nav-icon fa fa-sitemap"></i>
                <p>POHON JARINGAN</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?=site_url('site/user/deposit-detail')?>" class="nav-link">
                <i class="nav-icon fa fa-history"></i>
                <p>RIWAYAT</p>
              </a>
            </li>
            <?php
          }
          ?>
          <li class="nav-header">AKUN</li>
          <li class="nav-item">
            <a id="btn-changepass" href="<?=site_url('site/user/changepassword')?>" class="nav-link" data-title="Password">
              <i class="nav-icon fas fa-lock"></i>
              <p>UBAH PASSWORD</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=site_url('site/user/logout')?>" class="nav-link">
              <i class="nav-icon fa fa-sign-out"></i>
              <p>LOGOUT</p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <div class="content-wrapper">
    <?=$content?>
  </div>
  <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
          <b>Version</b> <?=$this->setting_web_version?>
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>. Strongly developed by <b>Partopi Tao</b>.
  </footer>
</div>
<div class="modal fade" id="confirmDialog" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">KONFIRMASI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i> BATAL</button>
        <button type="button" class="btn btn-sm  btn-primary btn-ok"><i class="far fa-check-circle"></i> LANJUT</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade modal-default" id="promptDialog" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
        <h4 class="modal-title">Prompt</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btn-flat btn-ok">OK</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-account" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">AKUN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        Loading...
      </div>
    </div>
  </div>
</div>
</body>
<!-- Bootstrap -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>

<!-- jQuery Mapael -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/raphael/raphael.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mapael/maps/world_countries.min.js"></script>
<!-- ChartJS -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js.old/Chart.min.js"></script>

<!-- FastClick -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/fastclick/fastclick.js"></script>
<!-- Sparkline -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/sparklines/sparkline.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
<!-- Select 2 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap select -->
<script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
<!-- Upload file -->
<!--<script src="<?=base_url()?>assets/js/jquery.uploadfile.min.js"></script>-->

<!-- Block UI -->
<script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/function.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.form.js"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- CK Editor -->
<script src="<?=base_url()?>assets/js/ckeditor/ckeditor.js"></script>

<!-- date-range-picker -->
<script src="<?=base_url()?>assets/js/moment.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.js"></script>

<!-- bootstrap color picker -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

<!-- JSPDF -->
<script src="<?=base_url()?>assets/js/jspdf/jspdf.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jspdf/libs/png_support/zlib.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jspdf/libs/png_support/png.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/from_html.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/addimage.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/png_support.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/split_text_to_size.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/standard_fonts_metrics.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/filesaver.js"></script>

<!-- HTML2CANVAS -->
<script src="<?=base_url()?>assets/js/html2canvas.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

<!-- InputMask -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/inputmask/jquery.inputmask.bundle.js"></script>

<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<script>
$(window).load(function() {
  $(".se-pre-con").fadeOut("slow");
});
$(document).ready(function(){
    $('[data-mask]').inputmask();
    $('.ui').button();
    $('a[href="<?=current_url()?>"]').addClass('active');
    $('.dropdown-menu textarea,.dropdown-menu input, .dropdown-menu label').click(function(e) {
        e.stopPropagation();
    });
    $(document).on('keypress','.angka',function(e){
        if((e.which <= 57 && e.which >= 48) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode==9 || e.which==43 || e.which==44 || e.which==45 || e.which==46 || e.keyCode==8){
            return true;
        }else{
            return false;
        }
    });
    $(document).on('blur','.uang',function(){
        $(this).val(desimal($(this).val(),0));
    }).on('focus','.uang',function(){
        $(this).val(toNum($(this).val()));
    });
    $(".uang").trigger("blur");

    var confirmDialog = $("#confirmDialog");
    confirmDialog.on("hidden.bs.modal", function(){
        $(".modal-body", confirmDialog).html("");
        $("button", confirmDialog).unbind();
    });

    if($('.cekboxaction').length){
      $('.cekboxaction').click(function(){
        var a = $(this);

        confirmDialog.on("hidden.bs.modal", function(){
          $(".modal-body", confirmDialog).html("");
        });

        if($('.cekbox:checked').length < 1){
          toastr.error("Belum ada data dipilih!");
          return false;
        }

        $(".modal-body", confirmDialog).html((a.data('confirm')||"Apakah anda yakin?"));
        confirmDialog.modal("show");
        $(".btn-ok", confirmDialog).click(function() {
          var thisBtn = $(this);
          thisBtn.html("Loading...").attr("disabled", true);
          $('#dataform').ajaxSubmit({
            dataType: 'json',
            url : a.attr('href'),
            success : function(data){
              if(data.error==0) {
                toastr.success(data.success);
                window.location.reload();
              }else {
                toastr.error(data.error);
              }
            },
            complete: function(){
              thisBtn.html("OK").attr("disabled", false);
              confirmDialog.modal("hide");
            }
          });
        });
        return false;
      });
    }

    $('a[href="<?=current_url()?>"]').addClass('active');
    $('a[href="<?=current_url()?>"]').closest('li.has-treeview').addClass('menu-open').children('.nav-link').addClass('active');
    //$('li.treeview.active').find('ul').eq(0).show();
    //$('li.treeview.active').find('.fa-angle-left').removeClass('fa-angle-left').addClass('fa-angle-down');

    $(".editor").wysihtml5();
    $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    //$("select").selectpicker();
    $('.datepicker').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
      locale: {
          format: 'Y-MM-DD'
      }
    });
    $('.datepicker').on('show.daterangepicker', function(ev, picker) {
      console.log("calendar is here");
    });

    /*$(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });*/
    $( ".alert-dismissible" ).fadeOut(3000, function() {
        // Animation complete.
    });
    //iCheck for checkbox and radio inputs
    /*$('input[type="checkbox"], input[type="radio"]').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });*/
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $('.colorpick').colorpicker();
    $('.colorpick').on('colorpickerChange', function(event) {
        $('.colorpick .fa-square').css('color', event.color.toString());
    });

    $(".money").number(true, 2, '.', ',');
    $(".uang").number(true, 0, '.', ',');
    $('#btn-changepass, #btn-changeprofile').click(function() {
      var href = $(this).attr('href');
      $('.modal-content', $('#modal-account')).load(href, function() {
        $('#modal-account').modal('show');
        $('.datepicker', $('#modal-account')).daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
          locale: {
              format: 'Y-MM-DD'
          }
        });
        $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
        $('form', $('#modal-account')).validate({
          submitHandler: function(form) {
            var btnSubmit = $('button[type=submit]', $(form));
            var txtSubmit = btnSubmit[0].innerHTML;
            btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
            $(form).ajaxSubmit({
              url: href,
              dataType: 'json',
              type : 'post',
              success: function(res) {
                if(res.error != 0) {
                  toastr.error(res.error);
                } else {
                  toastr.success(res.success);
                  $('#modal-account').modal('hide');
                  if(res.redirect) {
                    setTimeout(function(){
                      location.href = res.redirect;
                    }, 1000);
                  } else {
                    location.reload();
                  }

                }
              },
              error: function() {
                toastr.error('SERVER ERROR');
              },
              complete: function() {
                btnSubmit.html(txtSubmit);
              }
            });
            return false;
          }
        });
      });
      return false;
    });
});
</script>
</html>

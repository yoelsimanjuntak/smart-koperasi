<?php
$rkoperasi = $this->db
->where(COL_KOPISDELETED,0)
->get(TBL_M_KOPERASI)
->result_array();
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

  <title><?=$this->setting_web_name?> - <?=$this->setting_web_desc?></title>

  <!-- Bootstrap core CSS -->
  <link href="<?=base_url()?>assets/themes/digimedia/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


  <!-- Additional CSS Files -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/digimedia/assets/css/fontawesome.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/digimedia/assets/css/templatemo-digimedia-v3.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/digimedia/assets/css/animated.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/digimedia/assets/css/owl.css">
  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_logo?>>

  <style>
  .kop-profile li {
    opacity: 100 !important;
    position: relative !important;
    list-style: inherit !important;
    transform: none !important;
    color: #afafaf !important;
  }
  .kop-profile ol {
    padding-left: 25px !important
  }
  </style>
</head>
<body>
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>

  <!-- Pre-header Starts -->
  <div class="pre-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-sm-8 col-7">
          <ul class="info">
            <li><a href="#"><i class="fa fa-envelope"></i>info@smartkoperasi.com</a></li>
            <li><a href="#"><i class="fa fa-phone"></i>+62 853 5986 7032</a></li>
          </ul>
        </div>
        <div class="col-lg-4 col-sm-4 col-5">
          <ul class="social-media">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Pre-header End -->

  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="main-nav">
            <!-- ***** Logo Start ***** -->
            <a href="<?=site_url()?>" class="logo">
              <img src="<?=MY_IMAGEURL?>logo-header.png" alt="">
            </a>
            <!-- ***** Logo End ***** -->
            <!-- ***** Menu Start ***** -->
            <ul class="nav">
              <li class="scroll-to-section"><a href="#top" class="active">Beranda</a></li>
              <li class="scroll-to-section"><a href="#about">Tentang Kami</a></li>
              <li class="scroll-to-section"><a href="#services">Mitra Koperasi</a></li>
              <!--<li class="scroll-to-section"><a href="#portfolio">Projects</a></li>
              <li class="scroll-to-section"><a href="#blog">Blog</a></li>-->
              <li class="scroll-to-section"><a href="#contact">Kontak</a></li>
              <li><div class="border-first-button"><a href="<?=site_url('site/user/login')?>"><i class="fa fa-sign-in"></i>&nbsp;Login</a></div></li>
            </ul>
            <a class='menu-trigger'>
                <span>Menu</span>
            </a>
            <!-- ***** Menu End ***** -->
          </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- ***** Header Area End ***** -->

  <div class="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-6 align-self-center">
              <div class="left-content show-up header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                <div class="row">
                  <div class="col-lg-12">
                    <h6><?=$this->setting_web_name?></h6>
                    <h2><?=$this->setting_web_desc?></h2>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                  </div>
                  <div class="col-lg-12">
                    <div class="border-first-button scroll-to-section">
                      <a href="#contact">Kontak</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                <img src="<?=MY_IMAGEURL?>img-header.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="about" class="about section">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-6">
              <div class="about-left-image  wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                <img src="<?=MY_IMAGEURL?>img-header-2.png" alt="">
              </div>
            </div>
            <div class="col-lg-6 align-self-center  wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
              <div class="about-right-content">
                <div class="section-heading">
                  <h6>Tentang Kami</h6>
                  <h4>Smart <em>Koperasi</em></h4>
                  <div class="line-dec"></div>
                </div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
                <div class="row">
                  <div class="col-lg-4 col-sm-4">
                    <div class="skill-item first-skill-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0s">
                      <div class="progress" data-percentage="100">
                        <span class="progress-left">
                          <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                          <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">
                          <div>
                            100%<br>
                            <span>Mudah</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4">
                    <div class="skill-item second-skill-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0s">
                      <div class="progress" data-percentage="100">
                        <span class="progress-left">
                          <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                          <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">
                          <div>
                            100%<br>
                            <span>Canggih</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4">
                    <div class="skill-item third-skill-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0s">
                      <div class="progress" data-percentage="100">
                        <span class="progress-left">
                          <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                          <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">
                          <div>
                            100%<br>
                            <span>Akuntabel</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="services" class="services section">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-heading  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
            <h6>Mitra</h6>
            <h4><em>Koperasi</em> Terdaftar</h4>
            <div class="line-dec"></div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="naccs">
            <div class="grid">
              <div class="row">
                <div class="col-lg-12">
                  <div class="menu">
                    <?php
                    $no = 1;
                    foreach($rkoperasi as $kop) {
                      ?>
                      <div class="<?=$no==1?'first-thumb active':'thumb'?>">
                        <div class="thumb" style="padding: 30px 10px">
                          <span class="icon"><img src="<?=!empty($kop[COL_KOPLOGO])&&file_exists(MY_UPLOADPATH.'koperasi/'.$kop[COL_KOPLOGO])?MY_UPLOADURL.'koperasi/'.$kop[COL_KOPLOGO]:base_url().$this->setting_web_logo?>" alt="<?=$kop[COL_KOPNAMA]?>" /></span>
                          <span style="font-size: 10pt; text-align: left; padding-left: 5px"><?=$kop[COL_KOPNAMA]?></span>
                        </div>
                      </div>
                      <?php
                      $no++;
                    }
                    ?>
                  </div>
                </div>
                <div class="col-lg-12">
                  <ul class="nacc">
                    <?php
                    $no = 1;
                    foreach($rkoperasi as $kop) {
                      ?>
                      <li class="<?=$no==1?'active':''?>">
                        <div>
                          <div class="thumb">
                            <div class="row">
                              <div class="col-lg-3 align-self-center">
                                <div>
                                  <img src="<?=!empty($kop[COL_KOPLOGO])&&file_exists(MY_UPLOADPATH.'koperasi/'.$kop[COL_KOPLOGO])?MY_UPLOADURL.'koperasi/'.$kop[COL_KOPLOGO]:base_url().$this->setting_web_logo?>" alt="<?=$kop[COL_KOPNAMA]?>" style="width: 100%" />
                                </div>
                              </div>
                              <div class="col-lg-9 align-self-center">
                                <div class="left-text kop-profile">
                                  <h4><?=$kop[COL_KOPNAMA]?></h4>
                                  <?=$kop[COL_KOPPROFIL]?>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </li>
                      <?php
                      $no++;
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div id="free-quote" class="free-quote" style="display: none !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 offset-lg-4">
          <div class="section-heading  wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
            <h6>Get Your Free Quote</h6>
            <h4>Grow With Us Now</h4>
            <div class="line-dec"></div>
          </div>
        </div>
        <div class="col-lg-8 offset-lg-2  wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
          <form id="search" action="#" method="GET">
            <div class="row">
              <div class="col-lg-4 col-sm-4">
                <fieldset>
                  <input type="web" name="web" class="website" placeholder="Your website URL..." autocomplete="on" required>
                </fieldset>
              </div>
              <div class="col-lg-4 col-sm-4">
                <fieldset>
                  <input type="address" name="address" class="email" placeholder="Email Address..." autocomplete="on" required>
                </fieldset>
              </div>
              <div class="col-lg-4 col-sm-4">
                <fieldset>
                  <button type="submit" class="main-button">Get Quote Now</button>
                </fieldset>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="portfolio" class="our-portfolio section" style="display: none !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="section-heading wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <h6>Our Portofolio</h6>
            <h4>See Our Recent <em>Projects</em></h4>
            <div class="line-dec"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
      <div class="row">
        <div class="col-lg-12">
          <div class="loop owl-carousel">
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="<?=base_url()?>assets/themes/digimedia/assets/images/portfolio-01.jpg" alt="">
                </div>
                <div class="down-content">
                  <h4>Website Builder</h4>
                  <span>Marketing</span>
                </div>
              </div>
              </a>
            </div>
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="<?=base_url()?>assets/themes/digimedia/assets/images/portfolio-01.jpg" alt="">
                </div>
                <div class="down-content">
                  <h4>Website Builder</h4>
                  <span>Marketing</span>
                </div>
              </div>
              </a>
            </div>
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="<?=base_url()?>assets/themes/digimedia/assets/images/portfolio-02.jpg" alt="">
                </div>
                <div class="down-content">
                  <h4>Website Builder</h4>
                  <span>Marketing</span>
                </div>
              </div>
              </a>
            </div>
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="<?=base_url()?>assets/themes/digimedia/assets/images/portfolio-03.jpg" alt="">
                </div>
                <div class="down-content">
                  <h4>Website Builder</h4>
                  <span>Marketing</span>
                </div>
              </div>
              </a>
            </div>
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="<?=base_url()?>assets/themes/digimedia/assets/images/portfolio-04.jpg" alt="">
                </div>
                <div class="down-content">
                  <h4>Website Builder</h4>
                  <span>Marketing</span>
                </div>
              </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="blog" class="blog" style="display: none !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 offset-lg-4  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.3s">
          <div class="section-heading">
            <h6>Recent News</h6>
            <h4>Check Our Blog <em>Posts</em></h4>
            <div class="line-dec"></div>
          </div>
        </div>
        <div class="col-lg-6 show-up wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
          <div class="blog-post">
            <div class="thumb">
              <a href="#"><img src="<?=base_url()?>assets/themes/digimedia/assets/images/blog-post-01.jpg" alt=""></a>
            </div>
            <div class="down-content">
              <span class="category">SEO Analysis</span>
              <span class="date">03 August 2021</span>
              <a href="#"><h4>Lorem Ipsum Dolor Sit Amet, Consectetur Adelore
                Eiusmod Tempor Incididunt</h4></a>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doers itii eiumod deis tempor incididunt ut labore.</p>
              <span class="author"><img src="<?=base_url()?>assets/themes/digimedia/assets/images/author-post.jpg" alt="">By: Andrea Mentuzi</span>
              <div class="border-first-button"><a href="#">Discover More</a></div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
          <div class="blog-posts">
            <div class="row">
              <div class="col-lg-12">
                <div class="post-item">
                  <div class="thumb">
                    <a href="#"><img src="<?=base_url()?>assets/themes/digimedia/assets/images/blog-post-02.jpg" alt=""></a>
                  </div>
                  <div class="right-content">
                    <span class="category">SEO Analysis</span>
                    <span class="date">24 September 2021</span>
                    <a href="#"><h4>Lorem Ipsum Dolor Sit Amei Eiusmod Tempor</h4></a>
                    <p>Lorem ipsum dolor sit amet, cocteturi adipiscing eliterski.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="post-item">
                  <div class="thumb">
                    <a href="#"><img src="<?=base_url()?>assets/themes/digimedia/assets/images/blog-post-03.jpg" alt=""></a>
                  </div>
                  <div class="right-content">
                    <span class="category">SEO Analysis</span>
                    <span class="date">24 September 2021</span>
                    <a href="#"><h4>Lorem Ipsum Dolor Sit Amei Eiusmod Tempor</h4></a>
                    <p>Lorem ipsum dolor sit amet, cocteturi adipiscing eliterski.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="post-item last-post-item">
                  <div class="thumb">
                    <a href="#"><img src="<?=base_url()?>assets/themes/digimedia/assets/images/blog-post-04.jpg" alt=""></a>
                  </div>
                  <div class="right-content">
                    <span class="category">SEO Analysis</span>
                    <span class="date">24 September 2021</span>
                    <a href="#"><h4>Lorem Ipsum Dolor Sit Amei Eiusmod Tempor</h4></a>
                    <p>Lorem ipsum dolor sit amet, cocteturi adipiscing eliterski.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="contact" class="contact-us section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="section-heading wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
            <h6>KONTAK</h6>
            <h4>Hubungi <em>Kami</em></h4>
            <div class="line-dec"></div>
          </div>
        </div>
        <div class="col-lg-12 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
          <form id="contact" action="" method="post">
            <div class="row">
              <div class="col-lg-12">
                <div class="contact-dec">
                  <img src="<?=base_url()?>assets/themes/digimedia/assets/images/contact-dec-v3.png" alt="">
                </div>
              </div>
              <div class="col-lg-5">
                <div id="map">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.9835831962932!2d99.16396521457683!3d3.354161697558373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x303160405cde0763%3A0x3bbeabd38d85f1fc!2sMasjid%20Agung%20Kota%20Tebing%20Tinggi!5e0!3m2!1sen!2sid!4v1676958809804!5m2!1sen!2sid" width="100%" height="636px" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="fill-form">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="info-post">
                        <div class="icon">
                          <img src="<?=base_url()?>assets/themes/digimedia/assets/images/phone-icon.png" alt="">
                          <a href="#">+62 853 5986 7032</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="info-post">
                        <div class="icon">
                          <img src="<?=base_url()?>assets/themes/digimedia/assets/images/email-icon.png" alt="">
                          <a href="#">yoelrolas@gmail.com</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="info-post">
                        <div class="icon">
                          <img src="<?=base_url()?>assets/themes/digimedia/assets/images/location-icon.png" alt="">
                          <a href="#">Tebing Tinggi</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <fieldset>
                        <input type="name" name="name" id="name" placeholder="Name" autocomplete="on" required>
                      </fieldset>
                      <fieldset>
                        <input type="text" name="email" id="email" pattern="[^ @]*@[^ @]*" placeholder="Your Email" required="">
                      </fieldset>
                      <fieldset>
                        <input type="subject" name="subject" id="subject" placeholder="Subject" autocomplete="on">
                      </fieldset>
                    </div>
                    <div class="col-lg-6">
                      <fieldset>
                        <textarea name="message" type="text" class="form-control" id="message" placeholder="Message" required=""></textarea>
                      </fieldset>
                    </div>
                    <div class="col-lg-12">
                      <fieldset>
                        <button type="submit" id="form-submit" class="main-button ">Kirim Pesan</button>
                      </fieldset>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p>Copyright © <?=date('Y')?> <?=ucwords(strtolower($this->setting_web_name))?>.
          <br>Developed by <a href="https://www.linkedin.com/in/yoelrolas/" target="_blank" title="free css templates">Partopi Tao</a></p>
        </div>
      </div>
    </div>
  </footer>


  <!-- Scripts -->
  <script src="<?=base_url()?>assets/themes/digimedia/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/themes/digimedia/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets/themes/digimedia/assets/js/owl-carousel.js"></script>
  <script src="<?=base_url()?>assets/themes/digimedia/assets/js/animation.js"></script>
  <script src="<?=base_url()?>assets/themes/digimedia/assets/js/imagesloaded.js"></script>
  <script src="<?=base_url()?>assets/themes/digimedia/assets/js/custom.js"></script>

</body>
</html>

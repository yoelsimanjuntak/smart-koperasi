<?php
$ruser = GetLoggedUser();
$rmember = $this->db
->where(COL_UNIQ, $ruser[COL_IDMEMBER])
->get(TBL_T_ANGGOTA)
->row_array();
if(!empty($id)) {
  $rmember = $this->db
  ->where(COL_UNIQ, $id)
  ->get(TBL_T_ANGGOTA)
  ->row_array();
}

$rkoperasi = $this->db
->where(COL_UNIQ, $rmember[COL_IDKOPERASI])
->get(TBL_M_KOPERASI)
->row_array();
if(!empty($rkoperasi) && !empty($rkoperasi[COL_KOPLOGO]) && file_exists(MY_UPLOADPATH.'koperasi/'.$rkoperasi[COL_KOPLOGO])) {
  $logo = MY_UPLOADURL.'koperasi/'.$rkoperasi[COL_KOPLOGO];
} else {
  $logo = base_url().$this->setting_web_logo;
}

$root = array();
$arrchild1 = array();
$arrchild2 = array();

$rchild1 = $this->db
->where(COL_IDREFERRAL, $rmember[COL_UNIQ])
->get(TBL_T_ANGGOTA)
->result_array();
foreach($rchild1 as $c1) {
  $arrchild2 = array();
  $rchild2 = $this->db
  ->where(COL_IDREFERRAL, $c1[COL_UNIQ])
  ->get(TBL_T_ANGGOTA)
  ->result_array();
  foreach($rchild2 as $c2) {
    $arrchild2[] = array(
      'text'=>array('name'=>$c2[COL_ANGNAMA],'contact'=>array('val'=>'LIHAT', 'href'=>site_url('site/user/tree/'.$c2[COL_UNIQ]))),
      'image'=>!empty($c2[COL_ANGPROFILEPIC]&&file_exists(MY_UPLOADPATH.$c2[COL_ANGPROFILEPIC]))?MY_UPLOADURL.$c2[COL_ANGPROFILEPIC]:$logo,
      'HTMLclass'=>$c2[COL_IDSPONSOR]==$ruser[COL_IDMEMBER]?'node-sponsored':''
    );
  }
  if(count($arrchild2)<2) {
    for($i=0; $i<=2-count($arrchild2); $i++) {
      $arrchild2[] = array(
        'text'=>array('name'=>'(KOSONG)', 'contact'=>array('val'=>'TAMBAH', 'href'=>site_url('site/user/register/'.$c1[COL_UNIQ]))),
        'image'=>$logo,
        'HTMLclass'=>"node-empty"
      );
    }
  }
  $arrchild1[] = array(
    'text'=>array('name'=>$c1[COL_ANGNAMA]),
    'image'=>!empty($c1[COL_ANGPROFILEPIC]&&file_exists(MY_UPLOADPATH.$c1[COL_ANGPROFILEPIC]))?MY_UPLOADURL.$c1[COL_ANGPROFILEPIC]:$logo,
    'children'=>$arrchild2,
    'HTMLclass'=>$c1[COL_IDSPONSOR]==$ruser[COL_IDMEMBER]?'node-sponsored':''
  );
}
if(count($arrchild1)<2) {
  for($i=0; $i<=2-count($arrchild1); $i++) {
    $arrchild1[] = array(
      'text'=>array('name'=>'(KOSONG)', 'contact'=>array('val'=>'TAMBAH', 'href'=>site_url('site/user/register/'.$rmember[COL_UNIQ]))),
      'image'=>$logo,
      'HTMLclass'=>"node-empty"
    );
  }
}
$root = array(
  'text'=>array('name'=>$rmember[COL_ANGNAMA], 'contact'=>!empty($id)&&$id!=$ruser[COL_IDMEMBER]?array('val'=>'LIHAT REFERRAL', 'href'=>site_url('site/user/tree/'.$rmember[COL_IDREFERRAL])):''),
  'image'=>$logo,
  'children'=>$arrchild1
);
?>
<link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
<style>
/*.chart { height: 600px; margin: 5px; width: 900px; }*/
.Treant > .node {  }
.Treant > p { font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-weight: bold; font-size: 12px; }
.Treant > .node img {
  width: 60px;
}
.node-name { font-weight: bold; margin-bottom: 0 !important; padding-top: 5px !important}
.node-empty .node-name{ color: #dc3545 !important }
.node-sponsored { background-color: #D9F8C4 !important }

.nodeExample1 {
  padding: 2px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #ffffff;
  border: 1px solid #000;
  width: 200px;
  font-family: Tahoma;
  font-size: 12px;
}

.nodeExample1 img {
    margin-right:  10px;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-info">
          <div class="card-body">
            <div class="chart" id="member-tree"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  var config = {
    container: "#member-tree",
    animateOnInit: true,
    animation: {
      nodeAnimation: "easeOutBounce",
      nodeSpeed: 700,
      connectorsAnimation: "bounce",
      connectorsSpeed: 700
    },
    connectors: {
      type: 'step'
    },
    node: {
      HTMLclass: 'nodeExample1'
    }
  };
  chartcfg = [
      config,
      <?=json_encode($root)?>,
      <?=json_encode($arrchild1)?>,
      <?=json_encode($arrchild2)?>
  ];
  new Treant(chartcfg);
});

</script>

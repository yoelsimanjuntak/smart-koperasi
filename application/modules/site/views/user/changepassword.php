<form id="form-changepass" method="post" action="#">
  <div class="modal-header">
    <h5 class="modal-title font-weight-bold">Ubah Password</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close"></i></span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group row">
          <label class="control-label col-sm-5">PASSWORD LAMA</label>
          <div class="col-sm-7">
            <input type="password" class="form-control" name="OldPassword" placeholder="*****" required />
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-sm-5">PASSWORD BARU</label>
          <div class="col-sm-7">
            <input type="password" class="form-control" name="NewPassword" placeholder="*****" required />
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-sm-5">KONFIRMASI PASSWORD</label>
          <div class="col-sm-7">
            <input type="password" class="form-control" name="ConfirmPassword" placeholder="*****" required />
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-outline-success btn-sm btn-ok"><i class="far fa-check-circle"></i>&nbsp;LANJUT</button>
  </div>
</form>

<?php
$ruser = GetLoggedUser();
$rkoperasi = $this->db
->where(COL_UNIQ, $rsponsor[COL_IDKOPERASI])
->get(TBL_M_KOPERASI)
->row_array();
if(!empty($rkoperasi) && !empty($rkoperasi[COL_KOPLOGO]) && file_exists(MY_UPLOADPATH.'koperasi/'.$rkoperasi[COL_KOPLOGO])) {
  $logo = MY_UPLOADURL.'koperasi/'.$rkoperasi[COL_KOPLOGO];
} else {
  $logo = base_url().$this->setting_web_logo;
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-info">
          <form id="form-main" action="<?=current_url()?>" enctype="multipart/form-data">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="<?=COL_REGNAMA?>" placeholder="Nama Lengkap" />
                      </div>
                      <div class="col-sm-6">
                        <label>NIK</label>
                        <input type="text" class="form-control" name="<?=COL_REGNIK?>" placeholder="Nomor KTP" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" name="<?=COL_REGALAMAT?>" placeholder="Alamat Domisili"></textarea>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Email</label>
                        <input type="text" class="form-control" name="<?=COL_REGEMAIL?>" placeholder="Email Aktif" />
                      </div>
                      <div class="col-sm-6">
                        <label>No. HP</label>
                        <input type="text" class="form-control" name="<?=COL_REGNOHP?>" placeholder="08XXXXXXXXXX" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-4">
                        <img src="<?=$logo?>" style="width: 100%; border-radius: 8px; box-shadow: inset 0 0 0 1px rgb(31 30 36 / 10%)" />
                      </div>
                      <div class="col-sm-8">
                        <label>Foto Profil <small class="font-weight-light">(Opsional)</small></label>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="file">
                          <label class="custom-file-label" for="file">Pilih Gambar</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 pl-4">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Referral</label>
                        <input type="text" class="form-control" value="<?=$rreff[COL_ANGNAMA]?>" disabled />
                        <input type="hidden" name="<?=COL_IDREFERRAL?>" value="<?=$rreff[COL_UNIQ]?>" />
                      </div>
                      <div class="col-sm-6">
                        <label>Sponsor</label>
                        <input type="text" class="form-control" value="<?=$rsponsor[COL_ANGNAMA]?>" disabled />
                        <input type="hidden" name="<?=COL_IDSPONSOR?>" value="<?=$rsponsor[COL_UNIQ]?>" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Username</label>
                        <input type="text" class="form-control" name="<?=COL_REGUSERNAME?>" placeholder="Min. 6 karakter" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Password</label>
                        <input type="password" class="form-control" name="<?=COL_REGPASSWORD?>" placeholder="Masukkan Password Akun" />
                      </div>
                      <div class="col-sm-6">
                        <label>Ulangi Password</label>
                        <input type="password" class="form-control" name="ConfirmPassword"  placeholder="Ketik Ulang Password Akun" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary"><i class="far fa-user-plus"></i>&nbsp;DAFTAR</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  $('#form-main').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              setTimeout(function(){
                location.href = res.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>

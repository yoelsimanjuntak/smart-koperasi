<?php
$ruser = GetLoggedUser();
$numSaldo = $this->db
->select_sum(COL_SALJUMLAH)
->where(COL_IDANGGOTA, $ruser[COL_IDMEMBER])
->get(TBL_T_SALDO)
->row_array();

$numMember = $this->db
->where(COL_IDSPONSOR, $ruser[COL_IDMEMBER])
->count_all_results(TBL_T_ANGGOTA);

$numKoperasi = $this->db
->where(COL_KOPISDELETED, 0)
->count_all_results(TBL_M_KOPERASI);

$numUser = $this->db
->where(COL_ISACTIVE, 1)
->count_all_results(TBL__USERS);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLESUPER) {
        ?>
        <div class="col-lg-6 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($numKoperasi)?></h3>
              <p>Koperasi</p>
            </div>
            <div class="icon">
              <i class="fas fa-building"></i>
            </div>
            <a href="<?=site_url('site/master/koperasi')?>" class="small-box-footer">LIHAT DAFTAR <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($numUser)?></h3>
              <p>Pengguna Aktif</p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
            <a href="<?=site_url('site/master/user')?>" class="small-box-footer">LIHAT DAFTAR <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php
      }else if($ruser[COL_ROLEID]==ROLEUSER) {
        ?>
        <div class="col-lg-6 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($numSaldo[COL_SALJUMLAH])?></h3>
              <p>Simpanan</p>
            </div>
            <div class="icon">
              <i class="fas fa-coins"></i>
            </div>
            <a href="<?=site_url('site/user/history')?>" class="small-box-footer">RIWAYAT <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($numMember)?></h3>
              <p>Anggota Referensi</p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
            <a href="<?=site_url('site/user/members')?>" class="small-box-footer">LIHAT <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
    <div class="row">
      <?php
      if($ruser[COL_IDROLE]!=ROLESUPER) {
        $rkoperasi = $this->db
        ->where(COL_UNIQ, $ruser[COL_IDKOPERASI])
        ->get(TBL_M_KOPERASI)
        ->row_array();
        if(!empty($rkoperasi)) {
          $rkopdet = $this->db
          ->where(COL_IDKOPERASI, $rkoperasi[COL_UNIQ])
          ->get(TBL_M_KOPERASI_DET)
          ->result_array();
          ?>
          <div class="col-sm-6">
            <div class="card card-widget widget-user-2" style="border-top: 3px solid #17a2b8">
              <div class="widget-user-header d-flex align-items-center" style="border-bottom: 1px solid rgba(0,0,0,.125)">
                <div class="widget-user-image">
                  <img class="img-circle" src="<?=MY_UPLOADURL.'/koperasi/'.$rkoperasi[COL_KOPLOGO]?>" alt="<?=strtoupper($rkoperasi[COL_KOPNAMA])?>">
                </div>
                <div>
                  <h6 class="widget-user-username font-weight-bold ml-2" style="font-size: 14pt"><?=strtoupper($rkoperasi[COL_KOPNAMA])?></h6>
                  <h5 class="widget-user-desc font-weight-light text-sm ml-2 mb-0"><?=$rkoperasi[COL_KOPALAMAT]?></h5>
                </div>
              </div>
              <!--<div class="card-header">
                <h5 class="card-title font-weight-bold"><?=strtoupper($rkoperasi[COL_KOPNAMA])?></h5>
              </div>-->
              <div class="card-body p-0">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <p class="nav-link mb-0 pb-2">
                      No. Telp
                      <span class="float-right font-weight-bold"><?=$rkoperasi[COL_KOPNOTELP]?></span>
                    </p>
                  </li>
                  <?php
                  foreach($rkopdet as $det) {
                    ?>
                    <li class="nav-item">
                      <p class="nav-link mb-0 pb-2">
                        <?=$det[COL_KOPLABEL]?>
                        <span class="float-right font-weight-bold">
                          <?php
                          if(!empty($det[COL_KOPFILE])) {
                            echo '<a href="'.MY_UPLOADURL.'/koperasi/'.$det[COL_KOPFILE].'" target="_blank">'.$det[COL_KOPTEKS].'</a>';
                          } else {
                            echo $det[COL_KOPTEKS];
                          }
                          ?>
                        </span>
                      </p>
                    </li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>
      <div class="col-sm-6">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h5 class="card-title">PAPAN INFORMASI</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
                </tr>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

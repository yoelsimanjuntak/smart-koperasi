<form id="form-user" action="<?=current_url()?>" method="post">
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>USERNAME</label>
        <input type="text" name="<?=COL_USERNAME?>" class="form-control" value="<?=!empty($data)?$data[COL_USERNAME]:''?>" placeholder="johndoe" required <?=!empty($data)?'readonly':''?> />
      </div>
      <div class="col-sm-6">
        <label>EMAIL</label>
        <input type="email" name="<?=COL_EMAIL?>" class="form-control" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" placeholder="johndoe@xyz.com" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>KATEGORI</label>
    <select class="form-control" name="<?=COL_IDROLE?>" style="width: 100%" required>
      <option value="<?=ROLESUPER?>" <?=!empty($data)&&$data[COL_IDROLE]==ROLESUPER?'selected':''?>>Superuser</option>
      <option value="<?=ROLEADMIN?>" <?=!empty($data)&&$data[COL_IDROLE]==ROLEADMIN?'selected':''?>>Administrator</option>
    </select>
  </div>
  <div class="form-group div-koperasi">
    <label>KOPERASI</label>
    <select class="form-control" name="<?=COL_IDKOPERASI?>" style="width: 100%" required>
      <?=GetCombobox("select * from m_koperasi order by KopNama", COL_UNIQ, COL_KOPNAMA, (!empty($data)?$data[COL_IDKOPERASI]:null))?>
    </select>
  </div>
  <div class="form-group">
    <label>NAMA LENGKAP</label>
    <input type="text" name="<?=COL_NAME?>" class="form-control" value="<?=!empty($data)?$data[COL_NAME]:''?>" placeholder="John Doe" required />
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>PASSWORD <?=!empty($data)?'<br /><small id="info-password" class="text-muted font-italic">Isi jika ingin mengubah password</small>':''?></label>
        <input type="password" name="<?=COL_PASSWORD?>" class="form-control" placeholder="Isi Password akun anda" />
      </div>
      <div class="col-sm-6">
        <label>ULANGI PASSWORD <?=!empty($data)?'<br /><small id="info-password" class="text-muted font-italic">Isi jika ingin mengubah password</small>':''?></label>
        <input type="password" name="ConfirmPassword" class="form-control" placeholder="Ulangi Password akun anda" />
      </div>
    </div>
  </div>
  <div class="form-group text-right mt-4">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $("[name=IDRole]").change(function(){
    var role_ = $(this).val();
    if(role_ == '<?=ROLEADMIN?>') {
      $('.div-koperasi').show();
      $("[name=IDKoperasi]").attr('required', true);
    } else {
      $('.div-koperasi').hide();
      $("[name=IDKoperasi]").attr('required', false);
    }
  }).trigger('change');
  $('#form-user').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>

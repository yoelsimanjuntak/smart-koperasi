<?php
$logo = base_url().$this->setting_web_logo;
if(!empty($data[COL_KOPLOGO]) && file_exists(MY_UPLOADPATH.'koperasi/'.$data[COL_KOPLOGO])) {
  $logo = MY_UPLOADURL.'koperasi/'.$data[COL_KOPLOGO];
}

$rdet = $this->db
->where(COL_IDKOPERASI, $data[COL_UNIQ])
->get(TBL_M_KOPERASI_DET)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light">Rincian Koperasi</h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('site/master/koperasi')?>">Koperasi</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <ul class="nav nav-tabs" id="tabs-main" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="tab-general" data-toggle="pill" href="#general" role="tab"><i class="far fa-info-circle"></i>&nbsp;DATA UMUM</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="tab-more" data-toggle="pill" href="#more" role="tab"><i class="far fa-cog"></i>&nbsp;DATA TAMBAHAN</a>
          </li>
        </ul>
        <div class="tab-content pb-4" id="tabcontent">
          <div class="tab-pane fade active show" id="general" role="tabpanel">
            <div class="col-sm-12">
              <form id="form-main" action="<?=site_url('site/master/koperasi-edit/'.$data[COL_UNIQ])?>" class="p-2 pt-4">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" name="<?=COL_KOPNAMA?>" value="<?=$data[COL_KOPNAMA]?>" />
                    </div>
                    <div class="form-group">
                      <label>No. Telp</label>
                      <input type="text" class="form-control" name="<?=COL_KOPNOTELP?>" value="<?=$data[COL_KOPNOTELP]?>" />
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" name="<?=COL_KOPALAMAT?>"><?=$data[COL_KOPALAMAT]?></textarea>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-4">
                          <img src="<?=$logo?>" style="width: 100%; border-radius: 8px; box-shadow: inset 0 0 0 1px rgb(31 30 36 / 10%)" />
                        </div>
                        <div class="col-sm-8">
                          <label>Logo</label>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file" accept="image/*">
                            <label class="custom-file-label" for="file">Pilih Gambar</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-8 pl-4">
                    <div class="form-group">
                      <label>Profil</label>
                      <textarea class="ckeditor" rows="5" name="<?=COL_KOPPROFIL?>"><?=$data[COL_KOPPROFIL]?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-left: -16px; margin-right: -16px; border-top: 1px solid #dedede">
                  <div class="col-sm-12 p-2 pl-3">
                    <button type="submit" class="btn btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="more" role="tabpanel">
            <div class="col-sm-12">
              <?php
              if(!empty($rdet)) {
                ?>
                <form id="form-detail" action="<?=site_url('site/master/koperasi-detail-change/'.$data[COL_UNIQ])?>" class="p-2 pt-4">
                  <div class="row">
                    <div class="col-sm-12">
                      <?php
                      foreach($rdet as $d) {
                        ?>
                        <div class="form-group">
                          <div class="row">
                            <label class="control-label col-sm-2"><?=$d[COL_KOPLABEL]?></label>
                            <div class="col-sm-5">
                              <input type="text" class="form-control" name="_input_detail_<?=$d[COL_UNIQ]?>" value="<?=$d[COL_KOPTEKS]?>"/>
                            </div>
                            <div class="col-sm-3">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" name="_file_detail_<?=$d[COL_UNIQ]?>" accept="*">
                                <label class="custom-file-label" for="_file_detail_<?=$d[COL_UNIQ]?>">Pilih Lampiran</label>
                              </div>
                            </div>
                            <?php
                            if(!empty($d[COL_KOPFILE]) && file_exists(MY_UPLOADPATH.'koperasi/'.$d[COL_KOPFILE])) {
                              ?>
                              <div class="col-sm-2">
                                <a href="<?=MY_UPLOADURL.'koperasi/'.$d[COL_KOPFILE]?>" target="_blank" class="btn btn-success font-italic"><i class="far fa-download"></i>&nbsp;UNDUH</a>
                              </div>
                              <?php
                            }
                            ?>
                          </div>
                        </div>
                        <?php
                      }
                      ?>
                    </div>
                  </div>
                  <div class="row" style="margin-left: -16px; margin-right: -16px; border-top: 1px solid #dedede">
                    <div class="col-sm-12 p-2 pl-3">
                      <button type="submit" class="btn btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
                    </div>
                  </div>
                </form>
                <?php
              }
              ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  CKEDITOR.config.removeButtons = 'Save,Templates,NewPage,ExportPdf,Preview,Print,Find,Replace,SelectAll,Scayt,CopyFormatting,RemoveFormat,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Link,Unlink,Anchor,Image,Smiley,PageBreak,Iframe,TextColor,BGColor,Maximize,ShowBlocks,About';
  CKEDITOR.config.height = 200;

  $('#form-main').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = null;
      var txtSubmit = '';
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });
      return false;
    }
  });

  $('#form-detail').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = null;
      var txtSubmit = '';
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });
      return false;
    }
  });
});
</script>

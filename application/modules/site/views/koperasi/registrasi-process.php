<?php
$ruser = GetLoggedUser();
if(!empty($data[COL_REGPROFILEPIC]) && file_exists(MY_UPLOADPATH.$data[COL_REGPROFILEPIC])) {
  $pic = MY_UPLOADURL.$data[COL_REGPROFILEPIC];
} else {
  $pic = base_url().$this->setting_web_logo;
}
$numPayment = 0;
$numFeeAdm = !empty(GetSetting('AMT_ADMFEE'))?toNum(GetSetting('AMT_ADMFEE')):0;
$numBonusReff = !empty(GetSetting('AMT_BONUS_REFERRAL'))&&!empty(GetSetting('NUM_REFFDEPTH'))?toNum(GetSetting('AMT_BONUS_REFERRAL'))*toNum(GetSetting('NUM_REFFDEPTH')):0;
$numBonusSponsor = !empty(GetSetting('AMT_BONUS_SPONSOR'))?toNum(GetSetting('AMT_BONUS_SPONSOR')):0;
$numPayment = $numFeeAdm+$numBonusReff+$numBonusSponsor;
?>
<form id="form-main" action="<?=current_url()?>" method="post">
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" name="<?=COL_REGNAMA?>" class="form-control" placeholder="John Doe" value="<?=$data[COL_REGNAMA]?>" required />
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-6">
            <label>NIK</label>
            <input type="text" name="<?=COL_REGNIK?>" class="form-control" placeholder="Nomor KTP" value="<?=$data[COL_REGNIK]?>" required />
          </div>
          <div class="col-sm-6">
            <label>No. HP</label>
            <input type="text" class="form-control" name="<?=COL_REGNOHP?>" placeholder="08XXXXXXXXXX" value="<?=$data[COL_REGNOHP]?>" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>Alamat</label>
        <textarea class="form-control" name="<?=COL_REGALAMAT?>" placeholder="Alamat Domisili"><?=$data[COL_REGALAMAT]?></textarea>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-4">
            <img src="<?=$pic?>" style="width: 100%; border-radius: 8px; box-shadow: inset 0 0 0 1px rgb(31 30 36 / 10%)" />
          </div>
          <div class="col-sm-8">
            <label>Foto Profil <small class="font-weight-light">(Opsional)</small></label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="file">
              <label class="custom-file-label" for="file">Pilih Gambar</label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-6 pl-4">
      <div class="form-group">
        <label>Referral</label>
        <input type="text" class="form-control" value="<?=$rreff[COL_ANGNAMA]?>" disabled />
        <input type="hidden" name="<?=COL_IDREFERRAL?>" value="<?=$rreff[COL_UNIQ]?>" />
      </div>
      <div class="form-group">
        <label>Sponsor</label>
        <input type="text" class="form-control" value="<?=$rsponsor[COL_ANGNAMA]?>" disabled />
        <input type="hidden" name="<?=COL_IDSPONSOR?>" value="<?=$rsponsor[COL_UNIQ]?>" />
      </div>
      <div class="form-group">
        <label>Username</label>
        <input type="text" class="form-control" name="<?=COL_REGUSERNAME?>" placeholder="Min. 6 karakter" value="<?=$data[COL_REGUSERNAME]?>" required readonly />
      </div>
      <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="<?=COL_REGEMAIL?>" placeholder="Email Aktif" value="<?=$data[COL_REGEMAIL]?>" />
      </div>
    </div>
    <div class="col-sm-12 text-center bg-info pt-2 pb-1" style="border-radius: 8px; box-shadow: inset 0 0 0 1px rgb(31 30 36 / 10%)">
      <p class="font-italic text-sm mb-0">SETORAN AWAL:<br></p>
      <h4 class="font-weight-bold">Rp. <?=number_format($numPayment)?>,-</h4>
    </div>
    <div class="col-sm-12 mt-3">
      <p class="text-sm text-muted font-italic mb-0">CATATAN:</p>
      <ul class="text-sm text-muted font-italic">
        <li>Pastikan jumlah setoran anggota sesuai dengan nominal diatas.</li>
        <li>Jumlah saldo awal yang diterima anggota sebesar <strong>Rp. <?=number_format($numFeeAdm)?>,-</strong>.</li>
      </ul>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('#form-main').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);

          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              $('#modal-process').modal('hide');
              dt.DataTable().ajax.reload();
            }, 1000);
          }
        },
        error: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          
          toastr.error('SERVER ERROR');
        },
        finish: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>

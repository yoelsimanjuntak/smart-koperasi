<?php

class MY_Controller extends CI_Controller{
    public $setting_web_name;
    public $setting_web_desc;
    public $setting_web_logo;
    public $setting_web_preloader;
    public $setting_web_version;

    function __construct(){
        parent::__construct();
        $this->setting_web_name = GetSetting('VAR_APPNAME');
        $this->setting_web_desc = GetSetting('VAR_APPDESC');
        $this->setting_web_logo = GetSetting('PATH_LOGO');
        $this->setting_web_preloader = GetSetting('PATH_PRELOADER');
        $this->setting_web_version = GetSetting('VAR_APPVERSION');
    }
}

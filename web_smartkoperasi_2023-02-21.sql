# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: web_smartkoperasi
# Generation Time: 2023-02-21 04:52:20 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Superuser'),
	(2,'Administrator'),
	(3,'Pengguna');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `Uniq` bigint(10) unsigned NOT NULL,
  `SettingName` varchar(50) DEFAULT NULL,
  `SettingVal` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`Uniq`, `SettingName`, `SettingVal`)
VALUES
	(1,'VAR_APPNAME','SMART KOPERASI'),
	(2,'VAR_APPDESC','Platform Tata Kelola Koperasi Terpadu'),
	(3,'VAR_APPVERSION','1.0'),
	(5,'PATH_PRELOADER','assets/media/preloader/main.gif'),
	(6,'PATH_LOGO','assets/media/image/logo.png'),
	(10,'NUM_REFFDEPTH','7'),
	(11,'AMT_BONUS_REFERRAL','25000'),
	(12,'AMT_BONUS_SPONSOR','25000'),
	(13,'AMT_ADMFEE','25000');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDMember` bigint(10) unsigned DEFAULT NULL,
  `IDKoperasi` bigint(10) unsigned DEFAULT NULL,
  `IDRole` tinyint(1) NOT NULL,
  `Username` varchar(50) NOT NULL DEFAULT '',
  `Email` varchar(50) DEFAULT '',
  `Password` varchar(200) NOT NULL DEFAULT '',
  `Name` varchar(50) DEFAULT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(50) DEFAULT '',
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `IDMember`, `IDKoperasi`, `IDRole`, `Username`, `Email`, `Password`, `Name`, `IsActive`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,NULL,NULL,1,'superuser','superuser@smartkoperasi.com','2ec147f690c954d995f8e421503dc799','Partopi Tao',1,NULL,NULL,NULL,NULL),
	(3,1,3,3,'mahyudanil','mahyudanil@gmail.com','e10adc3949ba59abbe56e057f20f883e','H. MAHYUDANIL, S.IP., M.H',1,'superuser','2023-02-20 18:00:00','',NULL),
	(4,NULL,3,2,'admin.msm','admin.msm@smartkoperasi.com','e10adc3949ba59abbe56e057f20f883e','Kop. Masyarakat Syariah Mandiri',1,'superuser','2023-02-21 08:27:27','',NULL),
	(13,12,3,3,'yoelrolas','yoelrolas@gmail.com','e10adc3949ba59abbe56e057f20f883e','Yoel Rolas Simanjuntak',1,'admin.msm','2023-02-21 11:11:41','',NULL),
	(14,13,3,3,'rirismanik','rirismanik17@gmail.com','e10adc3949ba59abbe56e057f20f883e','Riris Manik',1,'admin.msm','2023-02-21 11:30:41','',NULL);

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_koperasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_koperasi`;

CREATE TABLE `m_koperasi` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KopNama` varchar(50) NOT NULL DEFAULT '',
  `KopSlug` varchar(50) NOT NULL DEFAULT '',
  `KopProfil` text,
  `KopAlamat` text,
  `KopNoTelp` varchar(50) DEFAULT NULL,
  `KopLogo` varchar(50) DEFAULT NULL,
  `KopIsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `m_koperasi` WRITE;
/*!40000 ALTER TABLE `m_koperasi` DISABLE KEYS */;

INSERT INTO `m_koperasi` (`Uniq`, `KopNama`, `KopSlug`, `KopProfil`, `KopAlamat`, `KopNoTelp`, `KopLogo`, `KopIsDeleted`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(3,'Koperasi Konsumen Masyarakat Syariah Mandiri','koperasi-konsumen-masyarakat-syariah-mandiri','<p><u><strong>Visi:</strong></u><br />\r\n&quot;Tercapainya kesejahteraan anggota koperasi berdasarkan azas gotong royong dan kekeluargaan.&quot;</p>\r\n\r\n<p><u><strong>Misi:</strong></u></p>\r\n\r\n<ol>\r\n	<li>Membantu memenuhi kebutuhan anggota koperasi;</li>\r\n	<li>Menghadirkan ruh koperasi untuk kepentingan seluruh anggota koperasi;</li>\r\n	<li>Menjadi mitra usaha bagi anggota koperasi; dan</li>\r\n	<li>Meningkatkan tata kelola manajemen koperasi.</li>\r\n</ol>\r\n','Jln. Gn. Lauser Blok F3 No.26 Kel. Tj. Marulak, Kec. Rambutan, Kota Tebing Tinggi\r\n','08126423686','KOP_MASYARAKATSYARIAHMANDIRI_Logo1.png',0,'superuser','2023-02-18 20:02:32','superuser','2023-02-18 22:23:27');

/*!40000 ALTER TABLE `m_koperasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_koperasi_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_koperasi_det`;

CREATE TABLE `m_koperasi_det` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDKoperasi` bigint(10) unsigned NOT NULL,
  `KopLabel` varchar(50) DEFAULT NULL,
  `KopTeks` text,
  `KopFile` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `m_koperasi_det` WRITE;
/*!40000 ALTER TABLE `m_koperasi_det` DISABLE KEYS */;

INSERT INTO `m_koperasi_det` (`Uniq`, `IDKoperasi`, `KopLabel`, `KopTeks`, `KopFile`)
VALUES
	(1,3,'Email','kopmasyarakatsyariah@gmail.com',NULL),
	(2,3,'NIK','1274020030056',NULL),
	(3,3,'NIB','1702230089314','KOP_MASYARAKATSYARIAHMANDIRI_NIB.pdf'),
	(4,3,'No. Badan Hukum','AHU-0000506.AH.01.29.TAHUN 2023','KOP_MASYARAKATSYARIAHMANDIRI_AHU.pdf'),
	(5,3,'NPWP','62.792.962.3-114.000','KOP_MASYARAKATSYARIAHMANDIRI_NPWP.pdf');

/*!40000 ALTER TABLE `m_koperasi_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table t_anggota
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_anggota`;

CREATE TABLE `t_anggota` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDKoperasi` bigint(10) unsigned NOT NULL,
  `IDReferral` bigint(10) DEFAULT NULL,
  `IDSponsor` bigint(10) DEFAULT NULL,
  `AngNama` varchar(50) NOT NULL DEFAULT '',
  `AngNIK` varchar(50) NOT NULL DEFAULT '',
  `AngNoHP` varchar(50) DEFAULT '',
  `AngAlamat` text,
  `AngKTP` varchar(200) DEFAULT NULL,
  `AngProfilePic` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `t_anggota` WRITE;
/*!40000 ALTER TABLE `t_anggota` DISABLE KEYS */;

INSERT INTO `t_anggota` (`Uniq`, `IDKoperasi`, `IDReferral`, `IDSponsor`, `AngNama`, `AngNIK`, `AngNoHP`, `AngAlamat`, `AngKTP`, `AngProfilePic`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,3,NULL,NULL,'H. MAHYUDANIL, S.IP., M.H','1276022303730001','08126423686','Jln. Gn. Lauser Blok F3 No.26, Kel. Tj. Marulak, Kec. Rambutan Kota Tebing Tinggi',NULL,NULL,'superuser','2023-02-20 18:00:00',NULL,NULL),
	(12,3,1,1,'Yoel Rolas Simanjuntak','1271031208950002','085359867032','Medan',NULL,'150.jpeg','admin.msm','2023-02-21 11:11:41',NULL,NULL),
	(13,3,12,1,'Riris Manik','11112087','085359867032','Medan',NULL,NULL,'admin.msm','2023-02-21 11:30:41',NULL,NULL);

/*!40000 ALTER TABLE `t_anggota` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table t_anggota_reg
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_anggota_reg`;

CREATE TABLE `t_anggota_reg` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDKoperasi` bigint(10) unsigned NOT NULL,
  `IDReferral` bigint(10) unsigned NOT NULL,
  `IDSponsor` bigint(10) unsigned NOT NULL,
  `RegNama` varchar(50) NOT NULL DEFAULT '',
  `RegNIK` varchar(50) NOT NULL DEFAULT '',
  `RegNoHP` varchar(50) DEFAULT '',
  `RegKTP` varchar(200) DEFAULT '',
  `RegAlamat` text,
  `RegProfilePic` varchar(200) DEFAULT NULL,
  `RegEmail` varchar(50) NOT NULL DEFAULT '',
  `RegUsername` varchar(50) NOT NULL DEFAULT '',
  `RegPassword` varchar(200) NOT NULL DEFAULT '',
  `RegIsTerdaftar` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `t_anggota_reg` WRITE;
/*!40000 ALTER TABLE `t_anggota_reg` DISABLE KEYS */;

INSERT INTO `t_anggota_reg` (`Uniq`, `IDKoperasi`, `IDReferral`, `IDSponsor`, `RegNama`, `RegNIK`, `RegNoHP`, `RegKTP`, `RegAlamat`, `RegProfilePic`, `RegEmail`, `RegUsername`, `RegPassword`, `RegIsTerdaftar`, `CreatedBy`, `CreatedOn`)
VALUES
	(9,3,1,1,'Yoel Rolas Simanjuntak','1271031208950002','085359867032','','Medan','150.jpeg','yoelrolas@gmail.com','yoelrolas','e10adc3949ba59abbe56e057f20f883e',1,'mahyudanil','2023-02-21 08:58:30'),
	(10,3,12,1,'Riris Manik','11112087','085359867032','','Medan',NULL,'rirismanik17@gmail.com','rirismanik','e10adc3949ba59abbe56e057f20f883e',1,'mahyudanil','2023-02-21 11:28:49');

/*!40000 ALTER TABLE `t_anggota_reg` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table t_jurnal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_jurnal`;

CREATE TABLE `t_jurnal` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDKoperasi` bigint(10) unsigned NOT NULL,
  `IDTransaksi` bigint(10) unsigned NOT NULL,
  `JurTanggal` date NOT NULL,
  `JurNomor` varchar(50) NOT NULL DEFAULT '',
  `JurPosisi` enum('D','K') NOT NULL DEFAULT 'D',
  `JurRekening` varchar(50) NOT NULL DEFAULT '',
  `JurJumlah` double NOT NULL,
  `JurKeterangan` text,
  `JurReferensi` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `t_jurnal` WRITE;
/*!40000 ALTER TABLE `t_jurnal` DISABLE KEYS */;

INSERT INTO `t_jurnal` (`Uniq`, `IDKoperasi`, `IDTransaksi`, `JurTanggal`, `JurNomor`, `JurPosisi`, `JurRekening`, `JurJumlah`, `JurKeterangan`, `JurReferensi`, `CreatedBy`, `CreatedOn`)
VALUES
	(13,3,5,'2023-02-21','T-00005','K','Biaya Adm. Pendaftaran',225000,'Pendaftaran Anggota an. Yoel Rolas Simanjuntak','Transaksi ID#5','admin.msm','2023-02-21 11:11:41'),
	(14,3,5,'2023-02-21','T-00005','D','Simpanan Anggota',25000,'Bonus Referral Anggota an. H. MAHYUDANIL, S.IP., M.H','Transaksi ID#5','admin.msm','2023-02-21 11:11:41'),
	(15,3,5,'2023-02-21','T-00005','D','Simpanan Anggota',25000,'Bonus Sponsor Anggota an. H. MAHYUDANIL, S.IP., M.H','Transaksi ID#5','admin.msm','2023-02-21 11:11:41'),
	(16,3,5,'2023-02-21','T-00005','D','Simpanan Anggota',25000,'Simpanan Awal Anggota an. Yoel Rolas Simanjuntak','Transaksi ID#5','admin.msm','2023-02-21 11:11:41'),
	(17,3,5,'2023-02-21','T-00005','D','Kas',150000,'Sisa Pendaftaran Anggota an. Yoel Rolas Simanjuntak','Transaksi ID#5','admin.msm','2023-02-21 11:11:41'),
	(18,3,6,'2023-02-21','T-00006','K','Biaya Adm. Pendaftaran',225000,'Pendaftaran Anggota an. Riris Manik','Transaksi ID#6','admin.msm','2023-02-21 11:30:41'),
	(19,3,6,'2023-02-21','T-00006','D','Simpanan Anggota',25000,'Bonus Referral Anggota an. Yoel Rolas Simanjuntak','Transaksi ID#6','admin.msm','2023-02-21 11:30:41'),
	(20,3,6,'2023-02-21','T-00006','D','Simpanan Anggota',25000,'Bonus Referral Anggota an. H. MAHYUDANIL, S.IP., M.H','Transaksi ID#6','admin.msm','2023-02-21 11:30:41'),
	(21,3,6,'2023-02-21','T-00006','D','Simpanan Anggota',25000,'Bonus Sponsor Anggota an. H. MAHYUDANIL, S.IP., M.H','Transaksi ID#6','admin.msm','2023-02-21 11:30:41'),
	(22,3,6,'2023-02-21','T-00006','D','Simpanan Anggota',25000,'Simpanan Awal Anggota an. Riris Manik','Transaksi ID#6','admin.msm','2023-02-21 11:30:41'),
	(23,3,6,'2023-02-21','T-00006','D','Kas',125000,'Sisa Pendaftaran Anggota an. Riris Manik','Transaksi ID#6','admin.msm','2023-02-21 11:30:41');

/*!40000 ALTER TABLE `t_jurnal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table t_saldo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_saldo`;

CREATE TABLE `t_saldo` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDAnggota` bigint(10) unsigned NOT NULL,
  `IDTransaksi` bigint(10) unsigned NOT NULL,
  `SalJumlah` double NOT NULL,
  `SalKeterangan` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `t_saldo` WRITE;
/*!40000 ALTER TABLE `t_saldo` DISABLE KEYS */;

INSERT INTO `t_saldo` (`Uniq`, `IDAnggota`, `IDTransaksi`, `SalJumlah`, `SalKeterangan`, `CreatedBy`, `CreatedOn`)
VALUES
	(10,1,5,25000,'Bonus Referral Anggota an. Yoel Rolas Simanjuntak','admin.msm','2023-02-21 11:11:41'),
	(11,1,5,25000,'Bonus Sponsor Anggota an. Yoel Rolas Simanjuntak','admin.msm','2023-02-21 11:11:41'),
	(12,12,5,25000,'Pendaftaran Anggota an. Yoel Rolas Simanjuntak','admin.msm','2023-02-21 11:11:41'),
	(13,12,6,25000,'Bonus Referral Anggota an. Riris Manik','admin.msm','2023-02-21 11:30:41'),
	(14,1,6,25000,'Bonus Referral Anggota an. Riris Manik','admin.msm','2023-02-21 11:30:41'),
	(15,1,6,25000,'Bonus Sponsor Anggota an. Riris Manik','admin.msm','2023-02-21 11:30:41'),
	(16,13,6,25000,'Pendaftaran Anggota an. Riris Manik','admin.msm','2023-02-21 11:30:41');

/*!40000 ALTER TABLE `t_saldo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table t_transaksi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_transaksi`;

CREATE TABLE `t_transaksi` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `TransTanggal` date NOT NULL,
  `TransNama` varchar(200) NOT NULL DEFAULT '',
  `TransJumlah` double NOT NULL,
  `TransKeterangan` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `t_transaksi` WRITE;
/*!40000 ALTER TABLE `t_transaksi` DISABLE KEYS */;

INSERT INTO `t_transaksi` (`Uniq`, `TransTanggal`, `TransNama`, `TransJumlah`, `TransKeterangan`, `CreatedBy`, `CreatedOn`)
VALUES
	(5,'2023-02-21','Pendaftaran Anggota',225000,'Pendaftaran Anggota ID#12','admin.msm','2023-02-21 11:11:41'),
	(6,'2023-02-21','Pendaftaran Anggota',225000,'Pendaftaran Anggota ID#13','admin.msm','2023-02-21 11:30:41');

/*!40000 ALTER TABLE `t_transaksi` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
